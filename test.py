from numpy.random import uniform
from griddata import griddata, __version__
import pylab as p

print 'griddata version ',__version__
npts = int(raw_input('enter # of random points to plot:'))
x = uniform(-2,2,npts);  y = uniform(-2,2,npts)
z = x*p.exp(-x**2-y**2)

# x, y, and z are now vectors containing nonuniformly sampled data.
# Define a regular grid and grid data to it.
nx = 51; ny = 41
xi, yi = p.meshgrid(p.linspace(-2,2,nx),p.linspace(-2,2,ny))
# masked=True mean no extrapolation, output is masked array.
zi = griddata(x,y,z,xi,yi,masked=True)

print 'min/max = ',zi.min(), zi.max()

# Contour the gridded data, plotting dots at the nonuniform data points.
CS = p.contour(xi,yi,zi,15,linewidths=0.5,colors=['k'])
CS = p.contourf(xi,yi,zi,15,cmap=p.cm.jet)
p.colorbar() # draw colorbar
p.scatter(x,y,marker='o',c='b',s=5)
p.xlim(-1.9,1.9)
p.ylim(-1.9,1.9)
p.title('griddata test (%d points)' % npts)
#p.savefig('griddata.png')
p.show()
