import numpy
from nnint import _seti, _setr, _natgridd

_paramdict = {'bI':'float','bJ':'float','dup':'int','ext':'int',
              'hor':'float','igr':'int','non':'int','nul':'float',
              'ver':'float'}

__version__ = "0.1.2"

def griddata(x,y,z,xi,yi,masked=False,fill_value=1.e30,**kwargs):
    """
 zi = griddata(x,y,z,xi,yi,**kwargs) fits a surface of the form z = f(x,y)
 to the data in the (usually) nonuniformly spaced vectors (x,y,z).
 griddata interpolates this surface at the points specified by (xi,yi)
 to produce zi. 
 xi and yi form a grid (usually produced by meshgrid).
 and must be monotone increasing, but not necessarily uniformly spaced.
 Uses a technique called "natural neighbor interpolation" (see e.g
 Sibson, R., A Brief Description of Natural Neighbor Interpolation,
 in Interpreting Multivariate Data, ed. by V. Barnett,
 John Wiley & Sons, New York, 1981, pp. 21-36) as implemented
 in the NCAR natgrid library (http://ngwww.ucar.edu/ngdoc/ng/ngmath/natgrid/nnhome.html).

 Parameters controlling interpolation (set in **kwargs)
 (For more details see
  http://ngwww.ucar.edu/ngdoc/ng/ngmath/natgrid/contents.html)

Parameter | parameter  |                     |               |
  name    |   type     |  description        | legal values  | default value
          |            |                     |               |
------------------------------------------------------------------------------
  bI      | float      | tautness factor that| >= 1.         | 1.5
          |            | increases the       |               |
          |            | effect of the       |               |
          |            | gradients as bI     |               |
          |            | increases.          |               |
------------------------------------------------------------------------------
  bJ      | float      | tautness factor that| >= 1.         | 7.0
          |            | decreases the       |               |
          |            | breadth of the      |               |
          |            | region affected by  |               |
          |            | the gradients.      |               |
------------------------------------------------------------------------------
  dup     | int        | controls whether    | 0 = no        | 0
          |            | duplicate input     | 1 = yes       |
          |            | coordinates are     |               |
          |            | allowed.            |               |
------------------------------------------------------------------------------
  ext     | int        | flags whether       | 0 = no        | 1
          |            | extrapolation is    | 1 = yes       |
          |            | allowed outside the |               |
          |            | convex hull.        |               |
------------------------------------------------------------------------------
  hor     | float      | Specifies the       | >= 0.         | 0.1*(extent of
          |            | amount of horizontal|               | X values in
          |            | overlap to be       |               | output grid)
          |            | included outside of |               |             
          |            | the current region. |               |             
------------------------------------------------------------------------------
  igr     | int        | flag indicating if  | 0 = no        | 0
          |            | nonlinear interp    | 1 = yes       |
          |            | is to be done.      |               |
-------------------------------------------------------------------------------
  non     | int        | flags whether in-   | 0 = yes       | 0
          |            | terpolated values   | 1 = no        |
          |            | are allowed to be   |               |
          |            | negative.           |               |
------------------------------------------------------------------------------
  nul     | float      | the value to be used| Any float     | 0.0
          |            | on output for points|               |
          |            | outside of the      |               |
          |            | convex hull when    |               |
          |            | extrapolation is not|               |
          |            | allowed.            |               |
------------------------------------------------------------------------------
  ver     | float      | Specifies the       | >= 0.         | 0.1*(extent of
          |            | amount of vertical  |               | Y values in the
          |            | overlap to be       |               | output grid)
          |            | included outside of |               |            
          |            | the current region. |               |            
------------------------------------------------------------------------------

 if masked=True (default False), no extrapolation is done outside the 
 convex hull defined by the data points, and a masked array with a fill value
 given by the 'fill_value' keyword (default 1.e30) is returned.
 This is equivalent to setting 'ext=0' and 'nul=fill_value' in **kwargs,
 and masking the output values that are equal to fill_value. 
 Setting masked=True will override the values of ext and nul 
 specified in **kwargs.
    """
    # check inputs (unpythonic, but necessary).
    try: 
        sizex = x.shape
    except:                        
        raise TypeError, 'x must be a rank-1 array'
    if len(sizex) > 1:
        raise TypeError, 'x must be a rank-1 array'
    if sizex[0] < 4:
        raise ValueError, 'x must have length > 3'
    try: 
        sizey = y.shape
    except:                        
        raise TypeError, 'y must be a rank-1 array'
    if len(sizey) > 1:
        raise TypeError, 'y must be a rank-1 array'
    if sizey[0] < 4:
        raise ValueError, 'y must have length > 3'
    try: 
        sizez = z.shape
    except:                        
        raise TypeError, 'z must be a rank-1 array'
    if len(sizez) > 1:
        raise TypeError, 'z must be a rank-1 array'
    if sizez[0] < 4:
        raise ValueError, 'z must have length > 3'
    if not sizex==sizey==sizez:
        raise ValueError, 'x, y and z must be rank-1 arrays of the same length'

    try:
        shapexi = xi.shape
    except:
        raise ValueError, 'xi must be a rank-2 array'
    try:
        shapeyi = yi.shape
    except:
        raise ValueError, 'yi must be a rank-2 array'
    if shapexi != shapeyi:
        raise ValueError, 'xi and yi must have same shape'   

    # if masked=True, set parameters so that no extrapolation
    # is done, and values outside convex hull are set to fill_value.
    if masked:
        kwargs['ext']=0
        kwargs['nul']=fill_value

    # override default natgrid internal parameters with **kwargs.
    for key, val in kwargs.iteritems():
        if _paramdict.has_key(key):
            if _paramdict[key] == 'int':
                _seti(key,val)
            elif _paramdict[key] == 'float':
                _setr(key,val)
        else:
            raise KeyError, 'illegal keyword argument to griddata'

    # cast input arrays to doubles (this makes a copy)
    x = x.astype('d')
    y = y.astype('d')
    z = z.astype('d')
    xo = xi[0,:].astype('d')
    yo = yi[:,0].astype('d')
    if min(xo[1:]-xo[0:-1]) < 0 or min(yo[1:]-yo[0:-1]) < 0:
        raise ValueError, 'output grid defined by xi,yi must be monotone increasing'
    # allocate array for output (buffer will be overwritten 
    # natgridd)
    zo = numpy.empty(xi.shape, 'd')

    _natgridd(x,y,z,xo,yo,zo)

    # if masked=True, make masked array.
    if masked:
        zo = numpy.ma.masked_values(zo,fill_value)

    return zo
