from distutils.core import setup, Extension
import glob

deps = glob.glob('src/*.c')

extensions = [Extension("nnint",deps,include_dirs = ['src'],)]
packages          = ['griddata']
package_dirs       = {'':'lib'}
setup(
  name              = "griddata",
  version           = "0.1.2",
  description       = "Interpolate irregularly spaced data to a grid",
  long_description  = """
  Interpolates irregularly spaced data to a regular grid using natural
  neighbor interpolation.  Based on the natgrid library
  (http://ngwww.ucar.edu/ngdoc/ng/ngmath/natgrid/nnhome.html).""",
  url               = "http://www.cdc.noaa.gov/people/jeffrey.s.whitaker/python/griddata.html",
  download_url      = "http://www.cdc.noaa.gov/people/jeffrey.s.whitaker/python/griddata-0.1.2.tar.gz",
  author            = "Jeff Whitaker",
  author_email      = "jeffrey.s.whitaker@noaa.gov",
  platforms         = ["any"],
  license           = "GPL",
  keywords          = ["python","interpolation"],
  classifiers       = ["Development Status :: 4 - Beta",
			           "Intended Audience :: Science/Research", 
			           "License :: OSI Approved", 
			           "Topic :: Scientific/Engineering :: Visualization",
			           "Topic :: Software Development :: Libraries :: Python Modules",
			           "Operating System :: OS Independent"],
  packages          = packages,
  package_dir       = package_dirs,
  ext_modules       = extensions)
